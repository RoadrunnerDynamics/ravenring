// ravenmsgmsg - minimal library for directly reading and writing WXF data buffers.
//
// This file implements reading and writing functionality in a plain and simple way.
//  This is slow for modern computers, maybe ok for microcontrollers.
//
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "ravenring/ravenmsg.h"

#if defined(_WIN32) // windows
	#define  0
	#ifndef IS_LITTLE_ENDIAN
		#define IS_LITTLE_ENDIAN 1
	#endif
#else // not-windows
	#ifndef IS_LITTLE_ENDIAN
		#if defined(__BYTE_ORDER__) && __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
		#define IS_LITTLE_ENDIAN 0
		#else
		#define IS_LITTLE_ENDIAN 1
		#endif
	#endif
#endif

// write header
i64 ravenmsg_write_start(ravenmsg *msg)
{
	// reset cursor
	if(msg->length < 2) {
		return -1;
	}
	// add message header
	msg->data[0] = '8';
	msg->data[1] = ':';
	msg->cursor = 2;
	return 2;
}

// buffer creation/destruction
ravenmsg *ravenmsg_create(i64 buffer_size)
{
	// allocate message and data buffer
	ravenmsg *msg = malloc(sizeof(ravenmsg) + buffer_size);
	if(msg == NULL) {
		return NULL;
	}

	msg->length = buffer_size;
	msg->max_length = buffer_size;
	msg->cursor = 0;
	msg->data = (i8 *) msg + sizeof(ravenmsg);

	// add message header
	ravenmsg_write_start(msg);
	return msg;
}

void ravenmsg_destroy(ravenmsg *msg)
{
	free(msg);
	msg = NULL;
}

/// Write functions:
extern inline i64 ravenmsg_write_vari64_(ravenmsg *msg, i64 value)
{
	do {
		// no bounds check performed here; caller must ensure enough space
		// copy the low 7 bits and advance
		msg->data[msg->cursor] = (value & 0x7F) | 0x80;
		value = value >> 7;
		msg->cursor += 1;
	} while(value > 0);  // branch not taken 95% of the time

	// unset the last leading bit
	msg->data[msg->cursor - 1] &= 0x7F;
	return 0;
}

extern inline i64 ravenmsg_vari64_size_(i64 value)
{
	i64 num_bytes = 0;
	do {
		// 7 bits per byte
		value = value >> 7;
		num_bytes += 1;
	} while(value > 0);  // branch not taken 95% of the time
	return num_bytes;
}

extern inline void ravenmsg_write_8_(ravenmsg *msg, u8 value)
{
	msg->data[msg->cursor] = value;
	msg->cursor += 1;
}

extern inline void ravenmsg_write_16_(ravenmsg *msg, u16 value)
{
#if IS_LITTLE_ENDIAN
	memcpy((msg->data + msg->cursor), &value, sizeof(value));
#else
	msg->data[msg->cursor + 0] = (value >> 0) & 0xFF;
	msg->data[msg->cursor + 1] = (value >> 8) & 0xFF;
#endif
	msg->cursor += 2;
}

extern inline void ravenmsg_write_32_(ravenmsg *msg, u32 value)
{
#if IS_LITTLE_ENDIAN
	memcpy((msg->data + msg->cursor), &value, sizeof(value));
#else
	msg->data[msg->cursor + 0] = (value >> 0) & 0xFF;
	msg->data[msg->cursor + 1] = (value >> 8) & 0xFF;
	msg->data[msg->cursor + 2] = (value >> 16) & 0xFF;
	msg->data[msg->cursor + 3] = (value >> 24) & 0xFF;
#endif
	msg->cursor += 4;
}

extern inline void ravenmsg_write_64_(ravenmsg *msg, u64 value)
{
#if IS_LITTLE_ENDIAN
	memcpy((msg->data + msg->cursor), &value, sizeof(value));
#else
	msg->data[msg->cursor + 0] = (value >> 0) & 0xFF;
	msg->data[msg->cursor + 1] = (value >> 8) & 0xFF;
	msg->data[msg->cursor + 2] = (value >> 16) & 0xFF;
	msg->data[msg->cursor + 3] = (value >> 24) & 0xFF;
	msg->data[msg->cursor + 4] = (value >> 32) & 0xFF;
	msg->data[msg->cursor + 5] = (value >> 40) & 0xFF;
	msg->data[msg->cursor + 6] = (value >> 48) & 0xFF;
	msg->data[msg->cursor + 7] = (value >> 56) & 0xFF;
#endif
	msg->cursor += 8;
}

i64 ravenmsg_write_fn(ravenmsg *msg, i8 *name, i64 name_length, i64 argc)
{
	i64 start = msg->cursor;
	// Check if we have enough space (worst case assumed)
	// 1 byte for 'f', 9 for vari64, 1 byte for 's', 9 for vari64, length for name
	i64 length = 1 + ravenmsg_vari64_size_(argc);
	if(msg->cursor + length > msg->length) {
		return -1;
	}
	ravenmsg_write_8_(msg, 'f');
	ravenmsg_write_vari64_(msg, argc);
	ravenmsg_write_sym(msg, name, name_length);
	return msg->cursor - start;
}

i64 ravenmsg_write_i8(ravenmsg *msg, i8 value)
{
	// Check if we have enough space
	if(msg->cursor + 2 > msg->length) {
		return -1;
	}
	ravenmsg_write_8_(msg, 'C');
	ravenmsg_write_8_(msg, value);
	return 2;
}

i64 ravenmsg_write_i16(ravenmsg *msg, i16 value)
{
	// Check if we have enough space
	if(msg->cursor + 3 > msg->length) {
		return -1;
	}
	ravenmsg_write_8_(msg, 'j');
	ravenmsg_write_16_(msg, value);
	return 3;
}

i64 ravenmsg_write_i32(ravenmsg *msg, i32 value)
{
	// Check if we have enough space
	if(msg->cursor + 5 > msg->length) {
		return -1;
	}
	ravenmsg_write_8_(msg, 'i');
	ravenmsg_write_32_(msg, value);
	return 5;
}

i64 ravenmsg_write_i64(ravenmsg *msg, i64 value)
{
	// Check if we have enough space
	if(msg->cursor + 9 > msg->length) {
		return -1;
	}
	ravenmsg_write_8_(msg, 'L');
	ravenmsg_write_64_(msg, value);
	return 9;
}

i64 ravenmsg_write_f64(ravenmsg *msg, f64 value)
{
	// Check if we have enough space
	if(msg->cursor + 9 > msg->length) {
		return -1;
	}
	ravenmsg_write_8_(msg, 'r');
	union {
		f64 d;
		u64 i;
	} u;
	u.d = value;
	ravenmsg_write_64_(msg, u.i);
	return 9;
}

extern inline i64 ravenmsg_write_buffer_(ravenmsg *msg, i8 *value, i64 length, i8 label)
{
	// Check if we have enough space
	i64 msg_len = 1 + ravenmsg_vari64_size_(length) + length;
	if(msg->cursor + msg_len > msg->length) {
		return -1;
	}
	ravenmsg_write_8_(msg, label);
	ravenmsg_write_vari64_(msg, length);
	memcpy(msg->data + msg->cursor, value, length);
	msg->cursor += length;
	return msg_len;
}

// write a string
i64 ravenmsg_write_string(ravenmsg *msg, i8 *value, i64 length)
{
	return ravenmsg_write_buffer_(msg, value, length, 'S');
}

// write a binary blob
i64 ravenmsg_write_bin(ravenmsg *msg, i8 *data, i64 length)
{
	return ravenmsg_write_buffer_(msg, data, length, 'B');
}

i64 ravenmsg_write_sym(ravenmsg *msg, i8 *name, i64 name_length)
{
	return ravenmsg_write_buffer_(msg, name, name_length, 's');
}

// write a bigi64
i64 ravenmsg_write_bigi64(ravenmsg *msg, i8 *value, i64 length)
{
	return ravenmsg_write_buffer_(msg, value, length, 'I');
}

// write a bigreal
i64 ravenmsg_write_bigreal(ravenmsg *msg, i8 *value, i64 length)
{
	return ravenmsg_write_buffer_(msg, value, length, 'R');
}

// write a packed array
//<packedarray> := Á^<packedarray-type>^<array-dims>^<packedarray-data>
// rank is length of dims
i64 ravenmsg_write_packedarray(ravenmsg *msg, ravenmsg_array_type array_type, i64 rank, i64 *dims, void *data)
{
	i64 start = msg->cursor;
	// Check if we have enough space
	if(rank < 1) {
		return -7; // rank must be non-zero
	}
	i64 data_elements = dims[0]; // number of data elements in the array
	i64 data_vari64_size = ravenmsg_vari64_size_(dims[0]);
	for(i64 i = 1; i < rank; i += 1) {
		data_elements *= dims[i];
		data_vari64_size += ravenmsg_vari64_size_(dims[i]);
	}
	i64 data_bytes = data_elements * ravenmsg_array_type_size[array_type];
	i64 length = 1 + 1 + ravenmsg_vari64_size_(rank) + data_vari64_size + data_bytes;
	if(msg->cursor + length > msg->length) {
		return -1;
	}

	// Write the message
	ravenmsg_write_8_(msg, 0xC1);
	ravenmsg_write_8_(msg, ravenmsg_array_type_encoding[array_type]);
	ravenmsg_write_vari64_(msg, rank);
	for(i64 i = 0; i < rank; i += 1) {
		ravenmsg_write_vari64_(msg, dims[i]);
	}
#if IS_LITTLE_ENDIAN
	// check for fp exceptions ("encoding fp exceptions i64o packed array")
	// TODO (alec): use simd to check for fp exceptions and copy faster
	switch(array_type) {
	case RAVEN_ARRAY_C32: data_elements *= 2; // 2x f32 per c32
		; // fallthrough
	case RAVEN_ARRAY_F32: {
		f32 *f32_msg = (f32 *) (msg->data + msg->cursor);
		f32 *f32 = data;
		for(i64 i = 0; i < data_elements; i += 1) {
			if(isnan(f32[i]) || isinf(f32[i])) {
				msg->cursor = start;
				return -8; // fp exceptions are not allowed
			}
			f32_msg[i] = f32[i];
		}
		msg->cursor += data_bytes;
		break;
	}
	case RAVEN_ARRAY_C64: data_elements *= 2; // 2x f64 per c64
		; // fallthrough
	case RAVEN_ARRAY_F64: {
		f64 *f64_msg = (f64 *) (msg->data + msg->cursor);
		f64 *f64 = data;
		for(i64 i = 0; i < data_elements; i += 1) {
			if(isnan(f64[i]) || isinf(f64[i])) {
				msg->cursor = start;
				return -8;  // fp exceptions are not allowed
			}
			f64_msg[i] = f64[i];
		}
		msg->cursor += data_bytes;
		break;
	}
		// i64 types have no fp exceptions to check; just memcpy
	case RAVEN_ARRAY_I8:
		; // fallthrough
	case RAVEN_ARRAY_I16:
		; // fallthrough
	case RAVEN_ARRAY_I32:
		; // fallthrough
	case RAVEN_ARRAY_I64: memcpy(msg->data + msg->cursor, data, data_bytes);
		msg->cursor += data_bytes;
		break;
	default: // invalid type for packed array (e.g. unsigned)
		msg->cursor = start;
		return -3;
	}
#else
	switch(array_type) {
	case RAVEN_ARRAY_I8: {
		memcpy(msg->data + msg->cursor, data, data_bytes);
		msg->cursor += data_bytes;
		break;
	}
	case RAVEN_ARRAY_I16: {
		i16 *i16d = data;
		for(i64 i = 0; i < data_elements; i += 1) {
			ravenmsg_write_16_(msg, i16d[i]);
		}
		break;
	}
	case RAVEN_ARRAY_I32: {
		i32 *i32d = data;
		for(i64 i = 0; i < data_elements; i += 1) {
			ravenmsg_write_32_(msg, i32d[i]);
		}
		break;
	}
	case RAVEN_ARRAY_I64: {
		i64 *i64d = data;
		for(i64 i = 0; i < data_elements; i += 1) {
			ravenmsg_write_64_(msg, i64d[i]);
		}
		break;
	}
	case RAVEN_ARRAY_C32: data_elements *= 2; // 2x f32 per c32
		; // fallthrough
	case RAVEN_ARRAY_F32: {
		f32 *f32d = data;
		for(i64 i = 0; i < data_elements; i += 1) {
			ravenmsg_write_32_(msg, *(u32 * )(f32d + i));
			f32 *copied = (f32 *) (msg->data + msg->cursor - 4);
			if(isnan(*copied) || isinf(*copied)) {
				msg->cursor = start;
				return -8; // fp exceptions are not allowed
			}
		}
		break;
	}
	case RAVEN_ARRAY_C64: data_elements *= 2; // 2x f64 per c64
		; // fallthrough
	case RAVEN_ARRAY_F64: {
		f64 *f64d = data;
		for(i64 i = 0; i < data_elements; i += 1) {
			ravenmsg_write_64_(msg, *(u64 * )(f64d + i));
			f64 *copied = (f64 *) (msg->data + msg->cursor - 8);
			if(isnan(*copied) || isinf(*copied)) {
				msg->cursor = start;
				return -8; // fp exceptions are not allowed
			}
		}
		break;
	}
	default:
		// invalid array type (e.g. unsigned) for packed array
		msg->cursor = start;
		return -3;
	}
#endif // IS_LITTLE_ENDIAN
	return msg->cursor - start;
}

i64 ravenmsg_write_numericarray(ravenmsg *msg, ravenmsg_array_type array_type, i64 rank, i64 *dims, void *data)
{
	i64 start = msg->cursor;

	// Check if we have enough space (assume worst case vari64 size)
	if(rank < 1) {
		return -7; // rank must be non-zero
	}
	i64 data_elements = dims[0]; // number of data elements in the array
	i64 data_vari64_size = ravenmsg_vari64_size_(dims[0]);
	for(i64 i = 1; i < rank; i += 1) {
		data_elements *= dims[i];
		data_vari64_size += ravenmsg_vari64_size_(dims[i]);
	}
	i64 data_bytes = data_elements * ravenmsg_array_type_size[array_type];
	i64 length = 1 + 1 + ravenmsg_vari64_size_(rank) + data_vari64_size + data_bytes;
	if(msg->cursor + length > msg->length) {
		return -1;
	}

	// Write the message
	ravenmsg_write_8_(msg, 0xC2);
	ravenmsg_write_8_(msg, ravenmsg_array_type_encoding[array_type]);
	ravenmsg_write_vari64_(msg, rank);
	for(i64 i = 0; i < rank; i += 1) {
		ravenmsg_write_vari64_(msg, dims[i]);
	}

#if IS_LITTLE_ENDIAN
	memcpy(msg->data + msg->cursor, data, data_bytes);
	msg->cursor += data_bytes;
#else
	switch(array_type) {
	case RAVEN_ARRAY_I8:
		; // fallthrough
	case RAVEN_ARRAY_U8: {
		memcpy(msg->data + msg->cursor, data, data_bytes);
		msg->cursor += data_bytes;
		break;
	}
	case RAVEN_ARRAY_I16:
		; // fallthrough
	case RAVEN_ARRAY_U16: {
		i16 *i16 = data;
		for(i64 i = 0; i < data_elements; i += 1) {
			ravenmsg_write_16_(msg, i16[i]);
		}
		break;
	}
	case RAVEN_ARRAY_C32:
		data_elements *= 2; // 2x f32 per c32
		; // fallthrough
	case RAVEN_ARRAY_I32:
		; // fallthrough
	case RAVEN_ARRAY_U32:
		; // fallthrough
	case RAVEN_ARRAY_F32: {
		i32 *i32 = data;
		for(i64 i = 0; i < data_elements; i += 1) {
			ravenmsg_write_32_(msg, i32[i]);
		}
		break;
	}
	case RAVEN_ARRAY_C64:
		data_elements *= 2; // 2x f64 per c64
		; // fallthrough
	case RAVEN_ARRAY_I64:
		; // fallthrough
	case RAVEN_ARRAY_U64:
		; // fallthrough
	case RAVEN_ARRAY_F64: {
		i64 *i64d = data;
		for(i64 i = 0; i < data_elements; i += 1) {
			ravenmsg_write_64_(msg, i64d[i]);
		}
		break;
	}
	default:
		// invalid array type
		msg->cursor = start;
		return -3;
	}
#endif // IS_LITTLE_ENDIAN
	return msg->cursor - start;
}

// write head of association
i64 ravenmsg_write_association(ravenmsg *msg, i64 length)
{
	i64 start = msg->cursor;
	// Check if we have enough space
	// assuming worst-case vari64 length of 9
	if(msg->cursor + 1 + 9 > msg->length) {
		return -1;
	}
	ravenmsg_write_8_(msg, 'A');
	ravenmsg_write_vari64_(msg, length);
	return msg->cursor - start;
}

// write association rule head
// assume two elements come after this (key, value).
i64 ravenmsg_write_assoc_rule(ravenmsg *msg)
{
	// Check if we have enough space
	if(msg->cursor + 1 > msg->length) {
		return -1;
	}
	ravenmsg_write_8_(msg, '-');
	return 1;
}

// write association rule delayed head
// assume two elements come after this (key, value).
i64 ravenmsg_write_assoc_rule_delayed(ravenmsg *msg)
{
	// Check if we have enough space
	if(msg->cursor + 1 > msg->length) {
		return -1;
	}
	ravenmsg_write_8_(msg, ':');
	return 1;
}

/// Read functions:
i64 ravenmsg_read_element_type(ravenmsg *msg, ravenmsg_element_type *e)
{
	if(msg->cursor >= msg->length) {
		return -2;
	}
	u8 encoded = msg->data[msg->cursor];
	for(i64 i = 0; i < 16; i += 1) {
		if(encoded == ravenmsg_element_type_encoding[i]) {
			*e = (ravenmsg_element_type) i;
			return 0;
		}
	}
	return -1;

}

// read a vari64
// assuming i64 is appropriate size on different platforms.
i64 ravenmsg_read_vari64_(ravenmsg *msg, i64 *value)
{
	i64 start = msg->cursor;
	if(msg->cursor >= msg->length) {
		goto error;
	}
	if(msg->data[msg->cursor] & 0x80) {
		// 1 byte, occurs 95% of the time
		*value = msg->data[msg->cursor];
		msg->cursor += 1;
		return 1;
	}
	*value = 0;
	for(i64 shift = 0; shift < (i64) sizeof(i64) * 8; shift += 7) {
		u8 b = msg->data[msg->cursor];
		*value |= ((i64) b & 0x7F) << shift;
		msg->cursor += 1;
		if(b < 0x80) {
			return msg->cursor - start;
		}
		if(msg->cursor >= msg->length) {
			goto error;
		}
	}
	// goto error
error:
	msg->cursor = start;
	return -2;
}

extern inline void _ravenmsg_read_8(ravenmsg *msg, u8 *value)
{
	*value = msg->data[msg->cursor];
	msg->cursor += 1;
}

extern inline void _ravenmsg_read_16(ravenmsg *msg, u16 *value)
{
#if IS_LITTLE_ENDIAN
	memcpy(value, (msg->data + msg->cursor), sizeof(u16));
#else
	*value = ((u16)msg->data[msg->cursor + 0] << 0) |
		 ((u16)msg->data[msg->cursor + 1] << 8);
#endif
	msg->cursor += 2;

}

extern inline void _ravenmsg_read_32(ravenmsg *msg, u32 *value)
{
#if IS_LITTLE_ENDIAN
	memcpy(value, msg->data + msg->cursor, sizeof(u32));
#else
	*value = ((u32)msg->data[msg->cursor + 0] << 0) |
		 ((u32)msg->data[msg->cursor + 1] << 8)  |
		 ((u32)msg->data[msg->cursor + 2] << 16) |
		 ((u32)msg->data[msg->cursor + 3] << 24);
#endif
	msg->cursor += 4;
}

extern inline void _ravenmsg_read_64(ravenmsg *msg, u64 *value)
{
#if IS_LITTLE_ENDIAN
	memcpy(value, msg->data + msg->cursor, sizeof(u64));
#else
	*value = ((u64)msg->data[msg->cursor + 0] << 0)  |
		 ((u64)msg->data[msg->cursor + 1] << 8)  |
		 ((u64)msg->data[msg->cursor + 2] << 16) |
		 ((u64)msg->data[msg->cursor + 3] << 24) |
		 ((u64)msg->data[msg->cursor + 4] << 32) |
		 ((u64)msg->data[msg->cursor + 5] << 40) |
		 ((u64)msg->data[msg->cursor + 6] << 48) |
		 ((u64)msg->data[msg->cursor + 7] << 56);
#endif
	msg->cursor += 8;
}

i64 ravenmsg_read_start(ravenmsg *msg)
{
	msg->cursor = 0; // reset cursor
	if(msg->cursor >= msg->length) {
		return -2; // we are at end
	}
	if(msg->data[msg->cursor] != '8' || msg->data[msg->cursor + 1] != ':') {
		if(msg->data[msg->cursor] == '8' && msg->data[msg->cursor + 1] == 'C'
				&& msg->data[msg->cursor + 2] == 'C') {
			return -10; // compressed data
		}
		return -1; // not a ravenmsg message
	}
	// otherwise header read was successful.
	msg->cursor += 2;
	return 2;
}

i64 ravenmsg_read_fn(ravenmsg *msg, i8 *name, i64 *name_length, i64 max_length, i64 *argc)
{
	i64 start = msg->cursor;
	if(msg->cursor >= msg->length) {
		return -2; // we are at end
	}

	if(msg->data[msg->cursor] != 'f') {
		return -1; // not a function
	}
	msg->cursor += 1;

	if(ravenmsg_read_vari64_(msg, argc) < 0) {
		msg->cursor = start;
		return -2; // size issue
	}

	// read function name
	i64 ret = ravenmsg_read_sym(msg, name, name_length, max_length);
	if(ret < 0) {
		msg->cursor = start;
		return ret;
	}
	return msg->cursor - start;
}

i64 ravenmsg_read_i8(ravenmsg *msg, i8 *value)
{
	if(msg->cursor + 2 > msg->length) {
		return -2; // we are at end
	}

	if(msg->data[msg->cursor] != 'C') {
		return -1; // not an i64
	}
	*value = msg->data[msg->cursor + 1];

	msg->cursor += 2;
	return 2;
}

i64 ravenmsg_read_i16(ravenmsg *msg, i16 *value)
{
	if(msg->cursor + 3 > msg->length) {
		return -2; // we are at end
	}
	if(msg->data[msg->cursor] != 'j') {
		return -1; // not an i64
	}
	msg->cursor += 1;
	_ravenmsg_read_16(msg, (u16 *) value);
	return 3;
}

i64 ravenmsg_read_i32(ravenmsg *msg, i32 *value)
{
	if(msg->cursor + 5 > msg->length) {
		return -2; // we are at end
	}
	if(msg->data[msg->cursor] != 'i') {
		return -1; // not an i64
	}
	msg->cursor += 1;
	_ravenmsg_read_32(msg, (u32 *) value);
	return 5;
}

i64 ravenmsg_read_i64(ravenmsg *msg, i64 *value)
{
	if(msg->cursor + 9 > msg->length) {
		return -2; // we are at end
	}
	if(msg->data[msg->cursor] != 'L') {
		return -1; // not an i64
	}
	msg->cursor += 1;
	_ravenmsg_read_64(msg, (u64 *) value);
	return 9;
}

i64 ravenmsg_read_f64(ravenmsg *msg, f64 *value)
{
	if(msg->cursor + 9 > msg->length) {
		return -2; // we are at end
	}
	if(msg->data[msg->cursor] != 'r') {
		return -1; // not a f64
	}
	msg->cursor += 1;
	union {
		f64 d;
		u64 i;
	} u;
	_ravenmsg_read_64(msg, &u.i);
	*value = u.d;
	return 9;
}

i64 ravenmsg_read_buffer_(ravenmsg *msg, i8 *value, i64 *length, i64 max_length, i8 type)
{
	i64 start = msg->cursor;
	if(msg->cursor >= msg->length) {
		return -2; // we are at end
	}
	if(msg->data[msg->cursor] != type) {
		return -1; // not a string
	}
	msg->cursor += 1;

	if(ravenmsg_read_vari64_(msg, length) < 0) {
		msg->cursor = start;
		return -2; // at end
	}

	if(*length > max_length) {
		msg->cursor = start;
		return -3; // string data too long
	}

	if(msg->cursor + *length > msg->length) {
		msg->cursor = start;
		return -2; // at end
	}

	/*
		Error when compiling on JC computer:

		/usr/include/x86_64-linux-gnu/bits/string_fortified.h:34:10: error: ‘__builtin___memcpy_chk’ specified bound between 18446744073709551488 and 18446744073709551615 exceeds maximum object size 9223372036854775807 [-Werror=stringop-overflow=]
		34 |   return __builtin___memcpy_chk (__dest, __src, __len, __bos0 (__dest));
	*/
	memcpy(value, msg->data + msg->cursor, (u32) *length);
	msg->cursor += *length;
	return msg->cursor - start;
}

i64 ravenmsg_read_str(ravenmsg *msg, i8 *value, i64 *length, i64 max_length)
{
	return ravenmsg_read_buffer_(msg, value, length, max_length, 'S');
}

i64 ravenmsg_read_bin(ravenmsg *msg, i8 *data, i64 *length, i64 max_length)
{
	return ravenmsg_read_buffer_(msg, data, length, max_length, 'B');
}

i64 ravenmsg_read_sym(ravenmsg *msg, i8 *name, i64 *name_length, i64 max_length)
{
	return ravenmsg_read_buffer_(msg, name, name_length, max_length, 's');
}

i64 ravenmsg_read_bigi64(ravenmsg *msg, i8 *value, i64 *length, i64 max_length)
{
	return ravenmsg_read_buffer_(msg, value, length, max_length, 'I');
}

i64 ravenmsg_read_bigreal(ravenmsg *msg, i8 *value, i64 *length, i64 max_length)
{
	return ravenmsg_read_buffer_(msg, value, length, max_length, 'R');
}

// Need to check for fp exceptions for fp types and return error -4
i64 ravenmsg_read_packedarray(ravenmsg *msg, ravenmsg_array_type *array_type, i64 *rank, i64 *dims, i64 max_rank,
		void *data, i64 max_bytes)
{
	i64 start = msg->cursor;

	/// Read in the element type (packed or numeric) and array type (i8, u8, ..)
	// check at least 4 things to read in (elem type, array type, rank, dim0)
	if(msg->cursor + 4 > msg->length) {
		return -2; // we are at end
	}
	// check the first i8acter for packed array type i8acter
	if((u8) msg->data[msg->cursor] != (u8) 0xC1) {
		return -1; // not a packed array
	}
	// find the array type by inverse table lookup
	i64 encoded_type = (u8) msg->data[msg->cursor + 1];
	i64 type_found = 0;
	for(i64 i = 0; i < 12; ++i) {
		if(encoded_type == ravenmsg_array_type_encoding[i]) {
			*array_type = (ravenmsg_array_type) i;
			type_found = 1;
			break;
		}
	}
	if(!type_found || // Invalid packed array types:
			*array_type == RAVEN_ARRAY_U8 || *array_type == RAVEN_ARRAY_U16
			|| *array_type == RAVEN_ARRAY_U32 || *array_type == RAVEN_ARRAY_U64) {
		return -5; // not a packed array
	}
	msg->cursor += 2;

	if(ravenmsg_read_vari64_(msg, rank) < 0) {
		msg->cursor = start;
		return -2; // at end
	}
	if(*rank > max_rank) {
		msg->cursor = start;
		return -3; // too many dimensions
	}
	if(*rank < 1) {
		return -7; // rank must be non-zero
	}

	// read in the dimensions
	for(i64 i = 0; i < *rank; i += 1) {
		if(ravenmsg_read_vari64_(msg, dims + i) < 0) {
			msg->cursor = start;
			return -2; // at end
		}
	}

	// read in the data
	i64 data_elements = dims[0];
	for(i64 i = 1; i < *rank; i += 1) {
		data_elements *= dims[i];
	}
	i64 data_bytes = ravenmsg_array_type_size[*array_type] * data_elements;
	if(data_bytes > max_bytes) {
		msg->cursor = start;
		return -3; // too long
	}
	if(msg->cursor + data_bytes > msg->length) {
		msg->cursor = start;
		return -2; // at end
	}

#if IS_LITTLE_ENDIAN
	// check for fp exceptions ("encoding fp exceptions i64o packed array")
	// TODO (alec): use simd to check for fp exceptions and copy faster
	switch(*array_type) {
	case RAVEN_ARRAY_C32: data_elements *= 2; // 2x f32 per c32
		; // fallthrough
	case RAVEN_ARRAY_F32: {
		f32 *f32_msg = (f32 *) (msg->data + msg->cursor);
		f32 *f32_data = data;
		for(i64 i = 0; i < data_elements; i += 1) {
			if(isnan(f32_msg[i]) || isinf(f32_msg[i])) {
				msg->cursor = start;
				return -8; // fp exceptions are not allowed
			}
			f32_data[i] = f32_msg[i];
		}
		break;
	}
	case RAVEN_ARRAY_C64: data_elements *= 2; // 2x f64 per c64
		; // fallthrough
	case RAVEN_ARRAY_F64: {
		f64 *f64_msg = (f64 *) (msg->data + msg->cursor);
		f64 *f64_data = data;
		for(i64 i = 0; i < data_elements; i += 1) {
			if(isnan(f64_msg[i]) || isinf(f64_msg[i])) {
				msg->cursor = start;
				return -8;  // fp exceptions are not allowed
			}
			f64_data[i] = f64_msg[i];
		}
		break;
	}
		// i64 types have no fp exceptions to check; just memcpy
	case RAVEN_ARRAY_I8:
		; // fallthrough
	case RAVEN_ARRAY_I16:
		; // fallthrough
	case RAVEN_ARRAY_I32:
		; // fallthrough
	case RAVEN_ARRAY_I64: memcpy(data, msg->data + msg->cursor, data_bytes);
		break;
	default: // invalid type for packed array (e.g. unsigned)
		msg->cursor = start;
		return -3;
	}
	msg->cursor += data_bytes;
#else // BIG_ENDIAN:
	switch(*array_type) {
	case RAVEN_ARRAY_I8: {
		memcpy(data, msg->data + msg->cursor, data_bytes);
		msg->cursor += data_bytes;
		break;
	}
	case RAVEN_ARRAY_I16: {
		u16 *i16 = data;
		for(i64 i = 0; i < data_elements; i += 1) {
			_ravenmsg_read_16(msg, i16 + i);
		}
		break;
	}
	case RAVEN_ARRAY_I32: {
		u32 *i32 = data;
		for(i64 i = 0; i < data_elements; i += 1) {
			_ravenmsg_read_32(msg, i32 + i);
		}
		break;
	}
	case RAVEN_ARRAY_I64: {
		u64 *i64d = data;
		for(i64 i = 0; i < data_elements; i += 1) {
			_ravenmsg_read_64(msg, i64d + i);
		}
		break;
	}
	case RAVEN_ARRAY_C32: data_elements *= 2; // 2x f32 per c32
		; // fallthrough
	case RAVEN_ARRAY_F32: {
		f32 *f32_data = data;
		for(i64 i = 0; i < data_elements; i += 1) {
			_ravenmsg_read_32(msg, (u32 *)(f32_data + i));
			float current = *(f32_data + i);
			if(isnan(current) || isinf(current)) {
				msg->cursor = start;
				return -8; // fp exceptions are not allowed
			}
		}
		break;
	}
	case RAVEN_ARRAY_C64: data_elements *= 2; // 2x f64 per c64
		; // fallthrough
	case RAVEN_ARRAY_F64: {
		f64 *f64_data = data;
		for(i64 i = 0; i < data_elements; i += 1) {
			_ravenmsg_read_64(msg, (u64 *)(f64_data + i));
			f64 current = *(f64_data + i);
			if(isnan(current) || isinf(current)) {
				msg->cursor = start;
				return -8; // fp exceptions are not allowed
			}
		}
		break;
	}
	default:
		// invalid array type (e.g. unsigned) for packed array
		msg->cursor = start;
		return -3;
	}
#endif // IS_LITTLE_ENDIAN
	return msg->cursor - start;
}

i64 ravenmsg_read_numericarray(ravenmsg *msg, ravenmsg_array_type *array_type, i64 *rank, i64 *dims, i64 max_rank,
		void *data, i64 max_bytes)
{
	i64 start = msg->cursor;
	if(msg->cursor >= msg->length) {
		return -2; // we are at end
	}

	// check the first i8acter for a numeric array type i8acter
	if((u8) msg->data[msg->cursor] != (u8) 0xC2) {
		return -1; // not a packed array
	}
	msg->cursor += 1;

	// read in the array type
	if(msg->cursor >= msg->length) {
		msg->cursor = start;
		return -2; // we are at end
	}
	i64 encoded_type = (u8) msg->data[msg->cursor];
	i64 type_found = 0;
	for(i64 i = 0; i < 12; ++i) {
		if(encoded_type == ravenmsg_array_type_encoding[i]) {
			*array_type = (ravenmsg_array_type) i;
			type_found = 1;
			break;
		}
	}
	if(!type_found) {
		msg->cursor = start;
		return -5; // not an array type
	}
	msg->cursor += 1;

	// check provided rank matches
	if(ravenmsg_read_vari64_(msg, rank) < 0) {
		msg->cursor = start;
		return -2; // at end
	}
	if(*rank > max_rank) {
		msg->cursor = start;
		return -3; // too many dimensions
	}
	if(*rank < 1) {
		return -7; // rank must be non-zero
	}

	// read in the dimensions
	for(i64 i = 0; i < *rank; ++i) {
		if(ravenmsg_read_vari64_(msg, dims + i) < 0) {
			msg->cursor = start;
			return -2; // at end
		}
	}

	// read in the data
	i64 data_elements = dims[0];
	for(i64 i = 1; i < *rank; i += 1) {
		data_elements *= dims[i];
	}
	i64 data_bytes = ravenmsg_array_type_size[*array_type] * data_elements;
	if(data_bytes > max_bytes) {
		msg->cursor = start;
		return -3; // too long
	}
	if(msg->cursor + data_bytes > msg->length) {
		msg->cursor = start;
		return -2; // at end
	}

#if IS_LITTLE_ENDIAN
	memcpy(data, msg->data + msg->cursor, data_bytes);
	msg->cursor += data_bytes;
#else
	switch(*array_type) {
	case RAVEN_ARRAY_I8:
		; // fallthrough
	case RAVEN_ARRAY_U8: {
		memcpy(data, msg->data + msg->cursor, data_bytes);
		msg->cursor += data_bytes;
		break;
	}
	case RAVEN_ARRAY_I16:
		; // fallthrough
	case RAVEN_ARRAY_U16: {
		u16 *u16d = (u16 *) data;
		for(i64 i = 0; i < data_elements; i += 1) {
			_ravenmsg_read_16(msg, u16d + i);
		}
		break;
	}
	case RAVEN_ARRAY_C32:
		data_elements *= 2; // c32 has 2x f32
		; // fallthrough
	case RAVEN_ARRAY_I32:
		; // fallthrough
	case RAVEN_ARRAY_U32:
		; // fallthrough
	case RAVEN_ARRAY_F32: {
		u32 *u32_data = (u32 *) data;
		for(i64 i = 0; i < data_elements; i += 1) {
			_ravenmsg_read_32(msg, u32_data + i);
		}
		break;
	}
	case RAVEN_ARRAY_C64:
		data_elements *= 2; // c64 has 2x f64
		; // fallthrough
	case RAVEN_ARRAY_I64:
		; // fallthrough
	case RAVEN_ARRAY_U64:
		; // fallthrough
	case RAVEN_ARRAY_F64: {
		u64 *u64_data = (u64 *) data;
		for(i64 i = 0; i < data_elements; i += 1) {
			_ravenmsg_read_64(msg, u64_data + i);
		}
		break;
	}
	default:
		// invalid array type
		msg->cursor = start;
		return -3;
	}
#endif
	return msg->cursor - start;
}

i64 ravenmsg_read_association(ravenmsg *msg, i64 *length)
{
	i64 start = msg->cursor;
	if(msg->cursor >= msg->length) {
		return -2; // we are at end
	}

	if(msg->data[msg->cursor] != 'A') {
		return -1; // not an association
	}
	msg->cursor += 1;

	if(ravenmsg_read_vari64_(msg, length) < 0) {
		msg->cursor = start;
		return -2; // at end
	}

	return msg->cursor - start;
}

i64 ravenmsg_read_assoc_rule(ravenmsg *msg)
{
	if(msg->cursor >= msg->length) {
		return -2; // we are at end
	}

	if(msg->data[msg->cursor] != '-') {
		return -1; // not a rule
	}
	msg->cursor += 1;
	return 1;
}

i64 ravenmsg_read_assoc_rule_delayed(ravenmsg *msg)
{
	if(msg->cursor >= msg->length) {
		return -2; // we are at end
	}

	if(msg->data[msg->cursor] != ':') {
		return -1; // not a ruledelayed
	}
	msg->cursor += 1;
	return 1;
}
