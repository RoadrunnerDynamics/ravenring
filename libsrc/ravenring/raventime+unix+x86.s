# Return the output of RDTSC, an x86 instruction, as a 64-bit integer.
.globl raventime_now_impl
.section .text

# Get the current processor time counter
raventime_now_impl:
	rdtsc
	shl $32, %rdx
	or %rdx, %rax
	ret
