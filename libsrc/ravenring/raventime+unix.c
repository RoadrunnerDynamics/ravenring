#include <time.h>

#include "ravenring/raventime.h"

// static calibration value for timer
static f64 NS_PER_TICK = 0.0;

// provided in asm, e.g., rdtsc
extern u64 raventime_now_impl(void);

raventime raventime_now(void) {
	raventime rt;
	rt.tick = raventime_now_impl();
	return rt;
}

// Calculate elapsed time since a start tick in nanoseconds
u64 raventime_elapsed(raventime start_time) {
	return raventime_elapsed_dt(raventime_now(), start_time);
}

// Calculate elapsed time between two ticks in nanoseconds
u64 raventime_elapsed_dt(raventime final_time, raventime start_time){
	if (NS_PER_TICK == 0.0) {
		raventime_calibrate(64e-6);
	}

	return (final_time.tick - start_time.tick) * NS_PER_TICK;
}

// Calculate elapsed time between two ticks in nanoseconds
void raventime_calibrate(f64 seconds) {
	// Save the start time
	struct timespec start_ts;
	clock_gettime(CLOCK_MONOTONIC, &start_ts);
	raventime start_tick = raventime_now();

	raventime_sleep(seconds);

	// Save the stop time
	struct timespec end_ts, end_ts_2;
	clock_gettime(CLOCK_MONOTONIC, &end_ts);
	raventime end_tick = raventime_now();
	clock_gettime(CLOCK_MONOTONIC, &end_ts_2);

	uint64_t measurement_ns = (end_ts_2.tv_sec - end_ts.tv_sec) * 1000000000
				+ end_ts_2.tv_nsec - end_ts.tv_nsec;

	// Compute elapsed time
	uint64_t elapsed_ticks = end_tick.tick - start_tick.tick;
	uint64_t elapsed_ns = (end_ts.tv_sec - start_ts.tv_sec) * 1000000000
				+ end_ts.tv_nsec - start_ts.tv_nsec - measurement_ns/2;

	// Update the calibration value
	NS_PER_TICK = (double) elapsed_ns / (double) elapsed_ticks;
}

// Sleep for a specified duration. Busy loop if the duration is small.
void raventime_sleep(f64 seconds) {
	u64 calibration_time_ns = seconds*1e9;
	if(calibration_time_ns >= 64000) {
		// Sleep using system timer for long durations
		// Compute sleep duration
		struct timespec sleep_dt = {
			.tv_sec = calibration_time_ns / 1000000000,
			.tv_nsec = calibration_time_ns % 1000000000
		};
		nanosleep(&sleep_dt, NULL);
	} else {
		struct timespec start_ts;
		clock_gettime(CLOCK_MONOTONIC, &start_ts);
		// Sleep using manual loop for short durations
		struct timespec end_ts_timer;
		u64 timer_elapsed = 0;
		while(timer_elapsed < calibration_time_ns)
		{
			clock_gettime(CLOCK_MONOTONIC, &end_ts_timer);
			timer_elapsed = (end_ts_timer.tv_sec - start_ts.tv_sec) * 1000000000
					+ end_ts_timer.tv_nsec - start_ts.tv_nsec;
		}
	}
}

// Sleep up to `max` if elapsed(`last_time`) > `min`
void raventime_nap(raventime last_time, f64 min, f64 max) {
	f64 elapsed = (1.0 / 1e9) * raventime_elapsed(last_time);
	if(elapsed > min) {
		// linearly interpolate sleep duration between max and min
		f64 sleep_duration = (elapsed + min) > max ? max : (elapsed + min);
		raventime_sleep(sleep_duration);
	}
}
