// Implementation of ravenring.h for unix

#include <stdlib.h> // calloc
#include <stdio.h>  // sprintf
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h> // TODO(Alec, 2024) custom memcpy

#include "ravenring/ravenring.h"
#include "ravenring/raventime.h"

#define WRITE_RESERVE_TIMEOUT 500*1000	// 500 us
#define WRITE_TIMEOUT 200*1000*1000 	// 100 ms

#define RAVENRING_SHM_FAILURE 100
#define RAVENRING_BAD_SIZE 200
#define RAVENRING_BUFFER_DEAD 300

// round up to the nearest 64 for cache size
inline i64 round_to_64(i64 i) {
	i64 anylowerbits = ((i&63) > 0);
	return (i & (~(63))) + 64*anylowerbits;
}

// create a ring buffer
ravenring* ravenring_create(
	char* topic,
	i64 topic_length,
	i64 message_size,
	i64 message_rate)
{
	i64 shm_fd = -1;
	// Get the topic as a null-terminated C string
	char topic_cstr[topic_length+1];
	sprintf(topic_cstr, "%.*s", (int) topic_length, topic);

	ravenring *ring = calloc(1, sizeof(ravenring));
	if(!ring) {
		errno = ENOMEM;
		goto ravenring_create_failure;
	}

	// Attempt to create a new shm segment, failing if it already exists
	shm_fd = shm_open(topic_cstr, O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);
	i32 exists = (shm_fd == -1 && errno == EEXIST);

	// Open the existing shared memory segment
	if(exists) {
		shm_fd = shm_open(topic_cstr, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
	}

	if(shm_fd == -1) {
		errno = RAVENRING_SHM_FAILURE;
		goto ravenring_create_failure;
	}

	// Compute ring buffer data size
	// buffer data starts after ring, aligned to 64 bytes
	i64 data_offset = round_to_64(sizeof(ravenring_shared));

	// We add 16 empty bytes to message for storing 8-byte size integer in front.
	message_size = round_to_64(16 + message_size);
	i64 required_buffer_size = round_to_64(message_size * message_rate); // size the ring buffer for 1s.
	i64 required_shm_size = data_offset + required_buffer_size;

	// This check is usually correct
	// TODO (Alec, 2024): rewrite in Ada SPARK
	if (required_buffer_size < 0 || message_size < 0 || message_rate < 0 ||
		message_size > 3037000499 || message_rate > 3037000499) // sqrt(INT_MAX)
	{
		errno = EINVAL;
		goto ravenring_create_failure;
	}

	if(exists) {
		printf("ring exists!!!!\n");
		raventime_sleep(1e-6); // Wait for possible creator to finish setup
		// ...

		// Check current size and resize if necessary
		struct stat shm_stat;
		if (fstat(shm_fd, &shm_stat) == -1) {
			// errno set by fstat
			goto ravenring_create_failure;
		}

		i32 needs_resize = shm_stat.st_size < required_shm_size;

		// Map shm to process address space
		ring->shared = mmap(NULL, required_shm_size, PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd, 0);
		if(ring->shared == MAP_FAILED) {
			// errno set my mmap
			goto ravenring_create_failure;
		}

		needs_resize |= ring->shared->message_size < message_size || ring->shared->length < message_rate
			|| ring->shared->message_size < 0 || ring->shared->length < 0;

		if(needs_resize) {
			errno = RAVENRING_BAD_SIZE;
			goto ravenring_create_failure;
		}
	} else {
		i64 ret = ftruncate(shm_fd, required_shm_size);
		if(ret == -1) {
			// ftruncate sets errno
			goto ravenring_create_failure;
		}
		ring->shared = mmap(NULL, required_shm_size, PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd, 0);
		if(ring->shared == MAP_FAILED) {
			errno = RAVENRING_SHM_FAILURE;
			goto ravenring_create_failure;
		}
		ring->shared->length = message_rate;
		ring->shared->message_size = message_size;
		ring->shared->reserve = 0;
		ring->shared->cursor = 0;
		ring->shared->update_time = 0;
		ring->shared->buffer_offset = data_offset;

		// Ring buffer data
		atomic_store_explicit(&ring->shared->dead, 0, memory_order_release);
	}

	ring->shm_handle = shm_fd;
	ring->shm_size = required_shm_size;

	return ring;

ravenring_create_failure:
	shm_unlink(topic_cstr);
	if(ring) {
		if(ring->shared != NULL) {
			munmap(ring->shared, required_shm_size);
			ring->shared = NULL;
		}
		free(ring);
	}
	if(shm_fd > 0) {
		close(shm_fd);
	}
	return 0;
}


i64 ravenring_revive(ravenring *ring) {
	printf("revive\n");
	if(!ring) {
		return -1;
	}

	// Return success if ring is already alive.
	if(ring->shared->dead == 0) {
		return 0;
	}
	if(ring->shared->dead < 0) {
		return -3;
	}

	// Sleep 1us to allow update_time to propagate
	raventime_sleep(1e-6);

	// Attempt to unlock a dead ring
	if(ring->shared->dead == 1) {
		i64 current_dead = 1;
		raventime_sleep(WRITE_TIMEOUT / 1.0e9);
		if(!atomic_compare_exchange_strong_explicit(&ring->shared->dead, &current_dead,
			2, memory_order_relaxed, memory_order_relaxed)) {
			return -2;
		}
		atomic_store_explicit(&ring->shared->update_time, raventime_now().tick, memory_order_release);
		ring->shared->reserve = ring->shared->cursor;
		atomic_store_explicit(&ring->shared->dead, 0, memory_order_release);
		return 0;
	}

	// if it has been 500ms since something has tried to unlock it,
	raventime rt = {ring->shared->update_time};
	if(raventime_elapsed(rt) > WRITE_TIMEOUT * 5) {
		i64 current_dead = ring->shared->dead;
		raventime_sleep(WRITE_TIMEOUT / 1.0e9);
		// if something else wrote to the update time before then, return.
		if(raventime_elapsed(rt) < WRITE_TIMEOUT * 5) {
			return -2;
		}
		// try to reserve the value
		if(!atomic_compare_exchange_strong_explicit(&ring->shared->dead, &current_dead,
			current_dead+1, memory_order_relaxed, memory_order_relaxed)) {
			return -2;
		}
		atomic_store_explicit(&ring->shared->update_time, raventime_now().tick, memory_order_release);
		ring->shared->reserve = ring->shared->cursor;
		atomic_store_explicit(&ring->shared->dead, 0, memory_order_release);
	}

	return 0;
}

ravenring *ravenring_connect(char* topic, i64 topic_length) {
	// Get the topic as a null-terminated C string
	char topic_cstr[topic_length+1];
	sprintf(topic_cstr, "%.*s", (int) topic_length, topic);

	// Attempt to open shared memory segment
	i64 shm_fd = shm_open(topic_cstr, O_RDWR, S_IRUSR | S_IWUSR);
	if(shm_fd < 0) {
		// errno set by shm_open
		return 0;
	}

	ravenring_shared *ring_shm = mmap(NULL, round_to_64(sizeof(ravenring_shared)), PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd, 0);
	if(ring_shm == MAP_FAILED) {
		shm_unlink(topic_cstr);
		close(shm_fd);
		// errno set by shm_open
		return 0;
	}

	// Wait for possible creator to finish setup
	raventime_sleep(1e-6);

	// Get the ring dimensions
	i64 message_size = ring_shm->message_size;
	i64 message_rate = ring_shm->length;

	i64 unmap_failed = munmap(ring_shm, round_to_64(sizeof(ravenring_shared)));
	if(unmap_failed) {
		// errno set by munmap
		return 0;
	}

	i64 shm_size = round_to_64(sizeof(ravenring_shared)) + round_to_64(message_size * message_rate);
	ring_shm = mmap(NULL, shm_size, PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd, 0);
	if(ring_shm == MAP_FAILED) {
		shm_unlink(topic_cstr);
		close(shm_fd);
		// errno set by shm_open
		return 0;
	}

	ravenring *ring = calloc(1, sizeof(ravenring));
	if(!ring) {
		errno = ENOMEM;
		shm_unlink(topic_cstr);
		munmap(ring_shm, shm_size);
		close(shm_fd);
		return 0;
	}

	ring->shm_handle = shm_fd;
	ring->shm_size = shm_size;
	ring->shared = ring_shm;

	return ring;
}

// Connect to a private ring buffer
ravenring *ravenring_connect_local(ravenring *private_ring) {
	if(!private_ring) {
		errno = EINVAL;
		return 0;
	}
	ravenring *ring = calloc(1, sizeof(ravenring));
	if(!ring) {
		errno = ENOMEM;
		return 0;
	}
	ring->shm_handle = 0;
	ring->shm_size = private_ring->shm_size;
	ring->shared = private_ring->shared;
	return ring;
}

// destroy a ring buffer
void ravenring_destroy(ravenring *ring)
{
	if (!ring) {
		return;
	}

	if(ring->shm_handle > 0) {
		// Get the segment path from the file descriptor
		char proc_path[32];
		snprintf(proc_path, sizeof(proc_path), "/proc/self/fd/%ld", ring->shm_handle);

		// Unlink the shared memory segment using the file descriptor
		unlink(proc_path);

		munmap(ring->shared, ring->shm_size);
		close(ring->shm_handle);
	} else if (ring->shm_handle == -2) {
		// This is a created private memory segment; unmap it.
		munmap(ring->shared, ring->shm_size);
	}
	// else, it is a connected private memory segment; don't attempt unmap

	free(ring);
	ring = 0;
}

i64 ravenring_write(ravenring *ring, i8 *data, i64 datasize)
{
	// Check ring not nullptr.
	if (!ring) {
		return -100;
	}

	// Try to unlock the ring.
	// By doing this here, we are making it so that the max time for
	//  this function to execute is 2*WRITE_TIMEOUT.
	if(ring->shared->dead) {
		if(ravenring_revive(ring) != 0) {
			return -100;
		}
	}

	// Check data and data size
	if (!data || datasize > ring->shared->message_size) {
		return -1;
	}

	raventime write_start = raventime_now();

	// Reserve a slot
	u64 reserve = ring->shared->reserve;
	while (!atomic_compare_exchange_weak_explicit(&ring->shared->reserve, &reserve,
			reserve+1, memory_order_relaxed, memory_order_relaxed)) {
		// Initial timeout of reserve after 500us
		if (raventime_elapsed(write_start) > WRITE_RESERVE_TIMEOUT) {
			return -2;
		}
	}

	// Optimization: we allow writing when reserve is less than 50% length away from cursor
	// Wait until we can write...
	while (reserve > ring->shared->cursor + ring->shared->length / 2) {
		// Timeout after 500ms seconds, writing dead and returning dead ring failure
		if (raventime_elapsed(write_start) > WRITE_TIMEOUT) {
			ring->shared->dead = 1;
			return -100;
		}
	}

	// Copy data:
	// index = (reserve % length), where length is an even power of two
	u64 index = reserve & (ring->shared->length-1);
	// get local memory mapped address of the buffer
	i8* buffer =  (i8*) ring->shared + ring->shared->buffer_offset;
	memcpy(buffer + ring->shared->message_size*index, &datasize, sizeof(i64));
	memcpy(buffer + ring->shared->message_size*index + 16, data, datasize);

	// Increment the cursor when it gets to our reserve value
	while(ring->shared->cursor != reserve) {
		if (raventime_elapsed(write_start) > WRITE_TIMEOUT) {
			ring->shared->dead = 1;
			return -100;
		}
	}
	ring->shared->update_time = raventime_now().tick;
	atomic_store_explicit(&ring->shared->cursor, reserve+1, memory_order_release);
	// Write fence

	return 0;
}

i64 ravenring_read(ravenring *ring, i8 *data, i64 max_size)
{
	raventime now = raventime_now();
	if(!ring || ring->shared->dead) {
		return -100; // dead ring should be unmapped
	}

	// update message parameters

	if (ring->shared->update_time == 0) {
		ring->read_time = now;
		return -1; // no data available yet
	}
	if(ring->read_time.tick == 0) {
		ring->read_time = now;
	}

	// Load the current cursor with a memory barrier
	u64 cursor = atomic_load_explicit(&ring->shared->cursor, memory_order_acquire);

	// If there is no new data, return -2
	if(ring->read_cursor == cursor) {
		ring->read_time = now;
		return -2;
	}

	// TODO(alec, 2024), should we advance if we are more than 0.5s behind?
	// This fixes certain problems when the ring could have reset or something
	// if(raventime_elapsed_dt(now, ring->read_time) > 400*1000*1000) {
	// 	ring->read_cursor = ring->shared->cursor - 1;
	// }

	// Advance if we are too far behind cursor
	if((i64)(cursor - ring->read_cursor) > ring->shared->length / 2) {
		ring->read_cursor = cursor - 1;
	}

	// Jump back if we are ahead of the ring cursor.
	if(ring->read_cursor > cursor) {
		ring->read_cursor = cursor - 1;
	}

	u64 slot = ring->read_cursor & (ring->shared->length-1);
	i64 current_message_size;

	// get local memory mapped address of the buffer
	i8 *buffer = (i8 *) ring->shared + ring->shared->buffer_offset;
	memcpy(&current_message_size, buffer + slot * ring->shared->message_size, sizeof(i64));

	// Sanity check that the message size is valid
	if(current_message_size <= 0 || current_message_size > ring->shared->message_size || current_message_size > max_size) {
		return -2;
	}

	// Write zeros to clear out any old data. Perhaps skip this if you want it to go faster.
	memset(data, 0, max_size);

	// Copy data from the ring buffer
	memcpy(data, buffer + slot * ring->shared->message_size + 16, current_message_size);

	ring->read_time = now;
	ring->read_cursor += 1;

	return current_message_size;
}

i64 ravenring_read_latest(ravenring *ring, i8 *data, i64 max_size)
{
	raventime now = raventime_now();
	u64 cursor = ring->shared->cursor;

	// If there is no new data, return -2
	if(ring->read_cursor == cursor) {
		ring->read_time = now;
		return -2;
	}

	// Otherwise, advance the cursor to the latest message and read it.
	ring->read_cursor = cursor - 1;
	return ravenring_read(ring, data, max_size);

}
