#ifndef raventime_h
#define raventime_h

#include <ravenring/style.h>

/// raventime - timing functions
// An un-calibrated clock counter value
typedef struct {
	i64 tick;
} raventime;

// Get the current uncalibrated processor timestamp counter
raventime raventime_now(void);

// Calculate elapsed time since a start tick in nanoseconds
u64 raventime_elapsed(raventime start_time);

// Calculate elapsed time between two ticks in nanoseconds
u64 raventime_elapsed_dt(raventime final_time, raventime start_time);

// Calibrate the timer for `seconds`, storing the result
void raventime_calibrate(f64 seconds);

// Sleep for a specified duration. Busy loop if the duration is small.
void raventime_sleep(f64 seconds);

// Sleep up to `max` if elapsed(`last_time`) > `min`
void raventime_nap(raventime last_time, f64 min, f64 max);

#endif
