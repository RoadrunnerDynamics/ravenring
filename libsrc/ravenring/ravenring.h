/// Ravenring - shared memory ring buffer message passing library

#ifndef ravenring_h
#define ravenring_h

#include <ravenring/style.h>
#include <ravenring/raventime.h>
#include <ravenring/ravenmsg.h>

#include <stdatomic.h>

typedef volatile atomic_uint_fast64_t atomic_u64;
typedef volatile atomic_int_fast64_t atomic_i64;

// Ring buffer
typedef struct {
	i64 length;
	i64 message_size;
	atomic_u64 reserve; // Writer reserve index
	atomic_u64 cursor; // Ring current head
	atomic_u64 update_time; // Last update tick (un-calibrated time counter)
	atomic_i64 dead; // Liveness indicator

	// Ring buffer data
	i64 buffer_offset;
} ravenring_shared;

// Wrapper for ring buffer that also includes reader parameters
typedef struct {
	// Reader parameters
	raventime read_time;
	u64 read_cursor;

	// Shared data
	i64 shm_handle;
	i64 shm_size;
	ravenring_shared *shared;
} ravenring;

// Create a named shared memory ring buffer and initialize it
ravenring* ravenring_create(char* topic, i64 topic_length, i64 message_size,
	i64 message_rate);

// Attempt to revive a dead ring
i64 ravenring_revive(ravenring *ring);

// Attempt to connect to an existing ring buffer, returning NULL on failure.
ravenring* ravenring_connect(char* topic, i64 topic_length);

// Connect to a ring available in the same process
ravenring* ravenring_connect_local(ravenring* ring);

// Delete a shared memory ring buffer
void ravenring_destroy(ravenring* ring);

// Copy a message into the buffer.
i64 ravenring_write(ravenring* ring, i8* msg, i64 msg_size);

// Copy the next message from the buffer
i64 ravenring_read(ravenring* ring, i8* msg, i64 max_size);

// Read the latest unread message from the buffer
i64 ravenring_read_latest(ravenring* ring, i8* msg, i64 max_size);

#endif // ravenring_h
