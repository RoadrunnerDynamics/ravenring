// ravenmsg - minimal library for directly reading and writing WXF data buffers.
#ifndef ravenmsg_h
#define ravenmsg_h

#include <ravenring/style.h>

/// Core types:
// struct to store a buffer for reading or writing encoded data
typedef struct {
	i64 max_length;
	i64 length;
	i64 cursor;
	i8 *data;
} ravenmsg;

// enum for element types
typedef enum {
	RAVEN_FUNCTION, // head / function
	RAVEN_i648, // signed 8-bit i64eger
	RAVEN_i6416, // signed 16-bit i64eger
	RAVEN_i6432, // signed 32-bit i64eger
	RAVEN_i6464, // signed 64-bit i64eger
	RAVEN_F64, // 64-bit floating poi64
	RAVEN_STR, // utf-8 string
	RAVEN_BINSTRING, // arbitrary binary data
	RAVEN_SYMBOL, // symbol
	RAVEN_BIGi64, // bigi64
	RAVEN_BIGREAL, // bigreal
	RAVEN_PACKEDARRAY, // packed array (efficient element storage, no NaN/Inf)
	RAVEN_NUMERICARRAY, // numeric array (numeric element storage)
	RAVEN_ASSOCIATION, // association (key-value dictionary)
	RAVEN_RULE, // association rule
	RAVEN_RULE_DELAYED, // association rule delayed
	RAVEN_NULL // null
} ravenmsg_element_type;

// enum for array types
typedef enum {
	RAVEN_ARRAY_I8, // signed 8-bit i64eger
	RAVEN_ARRAY_I16, // signed 16-bit i64eger
	RAVEN_ARRAY_I32, // signed 32-bit i64eger
	RAVEN_ARRAY_I64, // signed 64-bit i64eger
	RAVEN_ARRAY_U8, // unsigned 8-bit i64eger
	RAVEN_ARRAY_U16, // unsigned 16-bit i64eger
	RAVEN_ARRAY_U32, // unsigned 32-bit i64eger
	RAVEN_ARRAY_U64, // unsigned 64-bit i64eger
	RAVEN_ARRAY_F32, // 32-bit float
	RAVEN_ARRAY_F64, // 64-bit float
	RAVEN_ARRAY_C32, // complex (2x 32-bit float)
	RAVEN_ARRAY_C64 // complex (2x 64-bit float)
} ravenmsg_array_type;

/// Setup/teardown functions:
ravenmsg *ravenmsg_create(i64 buffer_size);
void ravenmsg_destroy(ravenmsg *msg);

/// Write functions
i64 ravenmsg_write_start(ravenmsg *msg);
i64 ravenmsg_write_fn(ravenmsg *msg, i8 *name, i64 name_length, i64 argc);
i64 ravenmsg_write_i8(ravenmsg *msg, i8 value);
i64 ravenmsg_write_i16(ravenmsg *msg, i16 value);
i64 ravenmsg_write_i32(ravenmsg *msg, i32 value);
i64 ravenmsg_write_i64(ravenmsg *msg, i64 value);
i64 ravenmsg_write_f64(ravenmsg *msg, f64 value);
i64 ravenmsg_write_string(ravenmsg *msg, i8 *value, i64 length);
i64 ravenmsg_write_bin(ravenmsg *msg, i8 *data, i64 length);
i64 ravenmsg_write_sym(ravenmsg *msg, i8 *name, i64 name_length);
i64 ravenmsg_write_bigi64(ravenmsg *msg, i8 *value, i64 length);
i64 ravenmsg_write_bigreal(ravenmsg *msg, i8 *value, i64 length);
i64 ravenmsg_write_packedarray(ravenmsg *msg, ravenmsg_array_type array_type,
		i64 rank, i64 *dims, void *data);
i64 ravenmsg_write_numericarray(ravenmsg *msg, ravenmsg_array_type array_type,
		i64 rank, i64 *dims, void *data);
i64 ravenmsg_write_association(ravenmsg *msg, i64 length);
i64 ravenmsg_write_assoc_rule(ravenmsg *msg);
i64 ravenmsg_write_assoc_rule_delayed(ravenmsg *msg);

/// Read functions
i64 ravenmsg_read_element_type(ravenmsg *msg, ravenmsg_element_type *e);
i64 ravenmsg_read_start(ravenmsg *msg);
i64 ravenmsg_read_fn(ravenmsg *msg, i8 *name, i64 *name_length, i64 max_name_length, i64 *argc);
i64 ravenmsg_read_i8(ravenmsg *msg, i8 *value);
i64 ravenmsg_read_i16(ravenmsg *msg, i16 *value);
i64 ravenmsg_read_i32(ravenmsg *msg, i32 *value);
i64 ravenmsg_read_i64(ravenmsg *msg, i64 *value);
i64 ravenmsg_read_f64(ravenmsg *msg, f64 *value);
i64 ravenmsg_read_str(ravenmsg *msg, i8 *value, i64 *length, i64 max_length);
i64 ravenmsg_read_bin(ravenmsg *msg, i8 *data, i64 *length, i64 max_length);
i64 ravenmsg_read_sym(ravenmsg *msg, i8 *name, i64 *name_length, i64 max_length);
i64 ravenmsg_read_bigi64(ravenmsg *msg, i8 *value, i64 *length, i64 max_length);
i64 ravenmsg_read_bigreal(ravenmsg *msg, i8 *value, i64 *length, i64 max_length);
i64 ravenmsg_read_packedarray(ravenmsg *msg, ravenmsg_array_type *array_type,
		i64 *rank, i64 *dims, i64 max_rank, void *data, i64 max_bytes);
i64 ravenmsg_read_numericarray(ravenmsg *msg, ravenmsg_array_type *array_type,
		i64 *rank, i64 *dims, i64 max_rank, void *data, i64 max_bytes);
i64 ravenmsg_read_association(ravenmsg *msg, i64 *length);
i64 ravenmsg_read_assoc_rule(ravenmsg *msg);
i64 ravenmsg_read_assoc_rule_delayed(ravenmsg *msg);

/// Utility data:
// table mapping element type to encoding value
static const u8 ravenmsg_element_type_encoding[16] = {
	'f', // function
	'C', // i8
	'j', // i16
	'i', // i32
	'L', // i64
	'r', // f64, f64, real
	'S', // String
	'B', // Binary string
	's', // symbol
	'I', // bigi64 (decimal string)
	'R', // bigreal
	0xC1, // packedarray
	0xC2, // numericarray
	'A', // Association
	'-', // rule
	':'  // delayed rule
};

// table for array type to encoding value
static const u8 ravenmsg_array_type_encoding[12] = {
	0x0, // RAVEN_ARRAY_I8
	0x1, // RAVEN_ARRAY_I16
	0x2, // RAVEN_ARRAY_I32
	0x3, // RAVEN_ARRAY_I64
	0x10, // RAVEN_ARRAY_U8		(NOT IN PACKED)
	0x11, // RAVEN_ARRAY_U16	(NOT IN PACKED)
	0x12, // RAVEN_ARRAY_U32	(NOT IN PACKED)
	0x13, // RAVEN_ARRAY_U64	(NOT IN PACKED)
	0x22, // RAVEN_ARRAY_F32
	0x23, // RAVEN_ARRAY_F64
	0x33, // RAVEN_ARRAY_C32
	0x34 // RAVEN_ARRAY_C64
};

// table for array type to size in bytes
static const u8 ravenmsg_array_type_size[12] = {
	1, // RAVEN_ARRAY_I8
	2, // RAVEN_ARRAY_I16
	4, // RAVEN_ARRAY_I32
	8, // RAVEN_ARRAY_I64
	1, // RAVEN_ARRAY_U8
	2, // RAVEN_ARRAY_U16
	4, // RAVEN_ARRAY_U32
	8, // RAVEN_ARRAY_U64
	4, // RAVEN_ARRAY_F32
	8, // RAVEN_ARRAY_F64
	8, // RAVEN_ARRAY_C32
	16 // RAVEN_ARRAY_C64
};

#endif // ravenmsg_h
