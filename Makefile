BUILD_PREFIX ?= build
INSTALL_PREFIX ?= install

LIB = libsrc/ravenring
CFLAGS = -Wall -Wextra -Wpedantic -Werror -O3 -static-libgcc -flto -fno-omit-frame-pointer -fstack-protector-all -D_FORTIFY_SOURCE=2 -ffunction-sections -fdata-sections -Wl,--gc-sections
CLFLAGS = -static
CTESTFLAGS = -fsanitize=address,undefined -lasan -lubsan

build: $(BUILD_PREFIX)/lib/libravenring.a compile_commands.json

$(BUILD_PREFIX)/lib/libravenring.a: $(LIB)/ravenring.h $(LIB)/ravenmsg.h $(LIB)/style.h $(LIB)/raventime.h $(LIB)/ravenmsg.c $(LIB)/ravenring+unix.c $(LIB)/raventime+unix.c
	mkdir -p $(BUILD_PREFIX)/lib
	gcc -c $(CFLAGS) $(CLFLAGS) -Ilibsrc $(LIB)/ravenmsg.c -o $(BUILD_PREFIX)/ravenmsg.o
	gcc -c $(LIB)/raventime+unix+x86.s -o $(BUILD_PREFIX)/raventime+unix+x86.o
	gcc -c $(CFLAGS) $(CLFLAGS) -Ilibsrc $(LIB)/raventime+unix.c -o $(BUILD_PREFIX)/raventime+unix.o
	gcc -c $(CFLAGS) $(CLFLAGS) -Ilibsrc $(LIB)/ravenring+unix.c -o $(BUILD_PREFIX)/ravenring+unix.o
	ar rcs $@ \
		$(BUILD_PREFIX)/raventime+unix+x86.o \
		$(BUILD_PREFIX)/ravenmsg.o \
		$(BUILD_PREFIX)/ravenring+unix.o \
		$(BUILD_PREFIX)/raventime+unix.o

compile_commands.json: Makefile
	@echo -e '[\n' \
	'{"directory": "$(PWD)", "command": "gcc -c $(CFLAGS) -Ilibsrc -o $(BUILD_PREFIX)/ravenmsg.o $(LIB)/ravenmsg.c", "file": "$(LIB)/ravenmsg.c"},\n' \
	'{"directory": "$(PWD)", "command": "gcc -c $(CFLAGS) -Ilibsrc -o $(BUILD_PREFIX)/raventime+unix.o $(LIB)/raventime+unix.c", "file": "$(LIB)/raventime+unix.c"},\n' \
	'{"directory": "$(PWD)", "command": "gcc -c $(CFLAGS) -Ilibsrc -o $(BUILD_PREFIX)/ravenring+unix.o $(LIB)/ravenring+unix.c", "file": "$(LIB)/ravenring+unix.c"},\n' \
	'{"directory": "$(PWD)", "command": "gcc $(CFLAGS) $(CTESTFLAGS) -Ilibsrc -o $(BUILD_PREFIX)/test/test_raventime test/test_raventime.c -L $(BUILD_PREFIX)/lib -lravenring", "file": "test/test_raventime.c"},\n' \
	'{"directory": "$(PWD)", "command": "gcc $(CFLAGS) $(CTESTFLAGS) -Ilibsrc -o $(BUILD_PREFIX)/test/test_ravenmsg test/test_ravenmsg.c -L $(BUILD_PREFIX)/lib -lravenring", "file": "test/test_ravenmsg.c"}' \
	'{"directory": "$(PWD)", "command": "gcc $(CFLAGS) $(CTESTFLAGS) -Ilibsrc -o $(BUILD_PREFIX)/test/test_ravenring_pubsub test/test_ravenring_pubsub.c -L $(BUILD_PREFIX)/lib -lravenring", "file": "test/test_ravenring_pubsub.c"}' \
	'{"directory": "$(PWD)", "command": "gcc $(CFLAGS) $(CTESTFLAGS) -Ilibsrc -o $(BUILD_PREFIX)/test/test_pub test/test_pub.c -L $(BUILD_PREFIX)/lib -lravenring", "file": "test/test_pub.c"}' \
	'{"directory": "$(PWD)", "command": "gcc $(CFLAGS) $(CTESTFLAGS) -Ilibsrc -o $(BUILD_PREFIX)/test/test_sub test/test_sub.c -L $(BUILD_PREFIX)/lib -lravenring", "file": "test/test_sub.c"}' \
	'\n]' > $@

check: build test/test_raventime.c test/test_ravenmsg.c test/test_ravenring_pubsub.c test/test_pub.c test/test_sub.c
	mkdir -p $(BUILD_PREFIX)/test
	gcc $(CFLAGS) $(CTESTFLAGS) -Ilibsrc -o $(BUILD_PREFIX)/test/test_raventime test/test_raventime.c -L $(BUILD_PREFIX)/lib -lravenring
	gcc $(CFLAGS) $(CTESTFLAGS) -Ilibsrc -o $(BUILD_PREFIX)/test/test_ravenmsg test/test_ravenmsg.c -L $(BUILD_PREFIX)/lib -lravenring
	gcc $(CFLAGS) $(CTESTFLAGS) -Ilibsrc -o $(BUILD_PREFIX)/test/test_ravenring_pubsub test/test_ravenring_pubsub.c -L $(BUILD_PREFIX)/lib -lravenring -lrt
	gcc $(CFLAGS) $(CTESTFLAGS) -Ilibsrc -o $(BUILD_PREFIX)/test/test_pub test/test_pub.c -L $(BUILD_PREFIX)/lib -lravenring -lrt
	gcc $(CFLAGS) $(CTESTFLAGS) -Ilibsrc -o $(BUILD_PREFIX)/test/test_sub test/test_sub.c -L $(BUILD_PREFIX)/lib -lravenring -lrt
	$(BUILD_PREFIX)/test/test_raventime
	$(BUILD_PREFIX)/test/test_ravenmsg
	$(BUILD_PREFIX)/test/test_ravenring_pubsub

install: $(BUILD_PREFIX)/lib/libravenring.a $(LIB)/ravenring.h $(LIB)/ravenmsg.h $(LIB)/raventime.h $(LIB)/style.h
	mkdir -p $(INSTALL_PREFIX)/lib
	mkdir -p $(INSTALL_PREFIX)/include/ravenring
	install -m 644 $(BUILD_PREFIX)/lib/libravenring.a $(INSTALL_PREFIX)/lib
	install -m 644 $(LIB)/*.h $(INSTALL_PREFIX)/include/ravenring

clean:
	rm -rf build install

.PHONY: build install check clean
