// publish test
#include <assert.h>
#include <string.h>
#include "ravenring/ravenring.h"

int main() {
	// Create shared memory ring buffer
	char *topic = "raventopic";
	int size = 32;  // bytes
	int rate =10000000;  // messages / s
	ravenring *raventopic = ravenring_create(topic, strlen(topic)-1, size, rate);
	assert(raventopic && "ring creation should not fail");

	// Allocate an empty message
	ravenmsg *msg = ravenmsg_create(size);
	assert(msg);

	uint64_t count = 0;
	while (1) {
		raventime start = raventime_now();
		// Build a message:
		// List["hello", count]
		ravenmsg_write_start(msg);
		ravenmsg_write_fn(msg, (int8_t*) "List", 4, 2);
		ravenmsg_write_string(msg, (int8_t*) "hello", 5);
		ravenmsg_write_i64(msg, count%(1024));

		// Publish the message
		ravenring_write(raventopic, msg->data, msg->cursor);

		// Sleep
		double elapsed = raventime_elapsed(start)*1e-9;
		double sleep_duration = 1.0/rate-elapsed < 0 ? 0 : 1.0/rate-elapsed;
		raventime_sleep(sleep_duration);

		count += 1;
	}
}
