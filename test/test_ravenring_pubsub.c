// A quick test of the publish and subscribe functionality of ravenring

#include "ravenring/raventime.h"
#include <stdio.h>
#include <assert.h>
#include <errno.h>
#include <string.h>

#include <limits.h>

#include <ravenring/ravenring.h>

int main() {
	printf("test ravenring Pub Sub\n");

	char *topic = "test_ravenring_pubsub";
	i64 topic_size = strlen(topic) - 1;

	ravenring *pub_ring = ravenring_create(topic, topic_size, 64, 64);
	assert(pub_ring && "test_ravenring_pubsub pub ring creation");

	// just make sure this does nothing
	ravenring_revive(pub_ring);
	assert(!pub_ring->shared->dead);

	ravenring *sub_ring = ravenring_connect(topic, topic_size);
	assert(sub_ring && "test_ravenring_pubsub sub ring connection");

	char *pub_msg = "hello world!";
	assert(ravenring_write(pub_ring, (i8*)pub_msg, strlen(pub_msg)) == 0
		&& "ravenring_write worked");
	printf("pub: %s\n", pub_msg);

	i8 read_buffer[64];
	i64 read_size = ravenring_read(sub_ring, read_buffer, 64);
	assert(read_size > 0 && "ravenring_read received data");
	printf("sub: %.*s\n", (int)read_size, (char*)read_buffer);

	ravenring_destroy(pub_ring);
	ravenring_destroy(sub_ring);

	printf("\t...completed\n");
	return 0;
}
