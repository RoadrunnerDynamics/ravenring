// test program for timing functions of raventime.h

#include <stdio.h>
#include <assert.h>

#include <ravenring/raventime.h>

int main() {
	raventime_calibrate(1e-3);
	raventime start = raventime_now();
	printf("test raventime...\n");
	raventime stop = raventime_now();
	u64 elapsed_1 = raventime_elapsed_dt(stop, start);
	u64 elapsed_2 = raventime_elapsed(start);
	assert(elapsed_2 - elapsed_1 < 1000 && "the measured times should be the same");
	assert(elapsed_1 < 1e9 && "The measured printing time should be fast");
	assert(elapsed_1 > 10 && "The measured printing time should not be that fast");
	printf("\t...completed\n");
}
