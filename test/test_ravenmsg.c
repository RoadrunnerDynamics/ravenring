// Test for ravenmsg format serialization/deserialization

#include <stdio.h>
#include <assert.h>

#include <ravenring/ravenmsg.h>

int main()
{
	printf("\ntest ravenmsg...\n");
	ravenmsg *msg = ravenmsg_create(1000);
	if(msg == NULL) {
		printf("Error creating message\n");
		return -1;
	}
	i64 ret = 0;
	// write a message:
	ret |= ravenmsg_write_fn(msg, (i8*)"List", 4, 11);
	ret |= ravenmsg_write_string(msg, (i8*)"Hello, World!", 13); // 1
	ret |= ravenmsg_write_string(msg, (i8*)"start", 5); // 2
	ret |= ravenmsg_write_i8(msg, 0x64); // 3
	ret |= ravenmsg_write_i16(msg, 0x6565); // 4
	ret |= ravenmsg_write_i32(msg, 0x66666666); // 5
	ret |= ravenmsg_write_i64(msg, 0x6767676767676767); // 6
	ret |= ravenmsg_write_f64(msg, 3.14159265358979323846264338327950); // 7
	int16_t data[] = {5, 6, 7, -128};
	i64 length = 4;
	i64 retn = ravenmsg_write_packedarray(msg, RAVEN_ARRAY_I16, 1, &length, (void *) data); // 8
	retn |= ravenmsg_write_numericarray(msg, RAVEN_ARRAY_U16, 1, &length, (void *) data); // 9
	assert(retn && "writing numeric array failed");

	ret |= ravenmsg_write_association(msg, 2); // 10
	ret |= ravenmsg_write_assoc_rule(msg);
	ret |= ravenmsg_write_string(msg, (i8*)"key1", 4);
	ret |= ravenmsg_write_string(msg, (i8*)"value1", 6);
	ret |= ravenmsg_write_assoc_rule_delayed(msg);
	ret |= ravenmsg_write_string(msg, (i8*)"key2", 4);
	ret |= ravenmsg_write_string(msg, (i8*)"value2", 6);
	ret |= ravenmsg_write_string(msg, (i8*)"end", 3);

	assert(ret);

	// print out the message
	printf("The message is: \t");
	for(int i = 0; i < msg->cursor; ++i) {
		printf("%c", msg->data[i]);
	}
	printf("\n");

	// print out each message byte in multiple formats
	// for(int i = 0; i < msg->cursor; i++) {
	// 	printf("%d\t%02x\t%c\n", (uint8_t) msg->data[i], msg->data[i],
	// 		msg->data[i]);
	// }

	// try reading:
	// int original_length = msg->length;
	msg->length = msg->cursor;
	msg->cursor = 0;
	ret = ravenmsg_read_start(msg);
	if(ret < 0) {
		printf("Error reading message header\n");
		return ret;
	}
	i8 name[20];
	i64 name_max_length = 20;
	i64 name_length;
	i64 argc = 0;
	ret = ravenmsg_read_fn(msg, name, &name_length, name_max_length, &argc);
	if(ret < 0) {
		printf("Error reading function head\n");
		return ret;
	}
	printf("Function name is %.*s\n", (int)name_length, name);
	printf("Function argc is %lu\n", argc);

	// read the arguments
	i64 readret = 0;
	readret |= ravenmsg_read_str(msg, name, &name_length, name_max_length);
	printf("Argument %d is %.*s\n", 1, (int)name_length, name);
	readret |= ravenmsg_read_str(msg, name, &name_length, name_max_length);
	printf("Argument %d is %.*s\n", 2, (int)name_length, name);
	int8_t i8d=0;
	readret |= ravenmsg_read_i8(msg, &i8d);
	printf("Argument %d is %x\n", 3, i8d);
	int16_t i16d=0;
	readret |= ravenmsg_read_i16(msg, &i16d);
	printf("Argument %d is %x\n", 4, i16d);
	int32_t i32d=0;
	readret |= ravenmsg_read_i32(msg, &i32d);
	printf("Argument %d is %x\n", 5, i32d);
	int64_t i64d=0;
	readret |= ravenmsg_read_i64(msg, &i64d);
	printf("Argument %d is %lx\n", 6, i64d);
	double d=0.0;
	readret |= ravenmsg_read_f64(msg, &d);
	printf("Argument %d is %f\n", 7, d);

	int16_t data_read[5] = {0};
	uint16_t data_read_2[5] = {0};
	int max_read_data_length = 5;
	ravenmsg_array_type t;
	i64 rank;
	i64 max_rank = 1;
	i64 dims;
	ret = ravenmsg_read_packedarray(msg, &t, &rank, &dims, max_rank, (void *) data_read,
			max_read_data_length * sizeof(int16_t));
	if(ret < 0) {
		printf("Error reading packed array\n");
		return ret;
	}
	assert(t == RAVEN_ARRAY_I16 && "numeric array type is signed");
	printf("Argument %d is ", 8);
	for(int i = 0; i < dims; ++i) {
		printf("%d ", data_read[i]);
	}
	printf("\n");

	ret = ravenmsg_read_numericarray(msg, &t, &rank, &dims, max_rank, (void *) data_read_2,
			max_read_data_length * sizeof(uint16_t));

	if(ret < 0) {
		printf("Error reading numeric array\n");
		return ret;
	}
	assert(t == RAVEN_ARRAY_U16 && "numeric array type is unsigned");
	printf("Argument %d is ", 9);
	for(int i = 0; i < dims; ++i) {
		printf("%d ", data_read_2[i]);
	}
	printf("\n");

	i64 association_length;
	ret = ravenmsg_read_association(msg, &association_length);
	if(ret < 0) {
		printf("Error reading association\n");
		return ret;
	}
	printf("Argument %d is association of length %ld\n", 10, association_length);

	// read first key, value strings
	readret |= ravenmsg_read_assoc_rule(msg);
	readret |= ravenmsg_read_str(msg, name, &name_length, name_max_length);
	printf("Key: %.*s\t", (int)name_length, name);
	readret |= ravenmsg_read_str(msg, name, &name_length, name_max_length);
	printf("Value: %.*s\n", (int)name_length, name);

	// read second key, value strings
	readret |= ravenmsg_read_assoc_rule_delayed(msg);
	readret |= ravenmsg_read_str(msg, name, &name_length, name_max_length);
	printf("Key: %.*s\t", (int)name_length, name);
	readret |= ravenmsg_read_str(msg, name, &name_length, name_max_length);
	printf("Value: %.*s\n", (int)name_length, name);

	// read end
	readret |= ravenmsg_read_str(msg, name, &name_length, name_max_length);
	printf("Argument 11 is %.*s\n", (int)name_length, name);

	ravenmsg_destroy(msg);
	assert(readret >= 0);

	printf("\t...completed\n");
	return 0;
}
