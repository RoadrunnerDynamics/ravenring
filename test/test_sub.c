#include <assert.h>
#include <string.h>
#include <stdio.h>

#include "ravenring/ravenring.h"

int main() {
	// Connect to our shared memory ring buffer
	char *topic = "raventopic";
	ravenring *raventopic = ravenring_connect(topic, strlen(topic)-1);
	assert(raventopic);

	// Allocate an empty message
	ravenmsg *msg = ravenmsg_create(raventopic->shared->message_size);
	assert(msg);

	raventime last_read = raventime_now();
	raventime last_second = raventime_now();

	uint64_t messages_recieved = 0;
	while (1) {
		// Print how many messages we read in the last second
		uint64_t nanoseconds = raventime_elapsed(last_second);
		if(nanoseconds > 1e9) {

			printf("%f messages per second\n",
				(double)messages_recieved/(nanoseconds*1e-9));
			last_second = raventime_now();
			messages_recieved = 0;
		}

		if (ravenring_read(raventopic, msg->data, msg->max_length) < 0) {
			// No new message, take a break
			raventime_nap(last_read, 100e-9, 1e-3);
			continue;
		}

		// Read succeeded
		// De-serialize our message
		// ["string", count]
		int8_t head[16];
		int64_t headsz = 16;
		int8_t str[16];
		int64_t strsz = 16;
		int64_t elements=0;
		int64_t count;

		int ret = 0;
		ret |= ravenmsg_read_start(msg);
		ret |= ravenmsg_read_fn(msg, head, &headsz, 16, &elements);
		assert(elements==2);
		ret |= ravenmsg_read_str(msg, str, &strsz, 16);
		ret |= ravenmsg_read_i64(msg, &count);
		assert(ret >= 0);

		messages_recieved += 1;
		last_read = raventime_now();
	}
}
